## Breast Cancer Dashboard
[click here](https://david21.shinyapps.io/bc-dashboard/) to view the Breast Canser dashboard

Breast Cancer Dashboard merupakan dashboard yang digunakan untuk memvisualisasi data breast cancer yang bersumber dari [Kaggle](https://www.kaggle.com/uciml/breast-cancer-wisconsin-data). Pengembangan dashboard ini menggunkaan library `shiny` pada bahasa pemrograman R.


Terdapat 4 fungsi utama pada dashboard ini yaitu:</br>

1. Summary
2. Ploting
3. Machine learning
4. Data


#### 1 Summary
Menu Summary memiliki fitur jumlah baris dan kolom pada data, histogram plot serta boxplot

#### 2 Ploting
Plot yang tersedia pada fitur ploting yaitu scatter plots dan Heat map

#### 3 Machine learning
Fitur yang ada pada Machine Learning yaitu pemodelan menggunakan KNN, Rendom Forest, dan Decision Tree. Hasil evaluasi dari model itu Accuracy, Recall, Precision, Specificity, Selain itu ada juga penjelasan singkat tentang metode yang dipakai serta plot dari model yang dibuat.

#### 4 Data
Fitur yang dimiliki pada menu **Data** yaitu menampilkan dataset yang digunakan, serta penjelasan tentang atribut yang ada pada data.</br>


### Fitur Selanjutnya
1. Menambahkan PCA dan Biplot pada machine learning
